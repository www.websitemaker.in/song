using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
namespace Affan
{
    #region User
    public class User
    {
        #region Member Variables
        protected string _name;
        protected string _email;
        protected string _file;
        protected int _id;
        #endregion
        #region Constructors
        public User() { }
        public User(string name, string email, string file)
        {
            this._name=name;
            this._email=email;
            this._file=file;
        }
        #endregion
        #region Public Properties
        public virtual string Name
        {
            get {return _name;}
            set {_name=value;}
        }
        public virtual string Email
        {
            get {return _email;}
            set {_email=value;}
        }
        public virtual string File
        {
            get {return _file;}
            set {_file=value;}
        }
        public virtual int Id
        {
            get {return _id;}
            set {_id=value;}
        }
        #endregion
    }
    #endregion
}